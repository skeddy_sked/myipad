import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser , InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { Http } from '@angular/http';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

options : InAppBrowserOptions = {
    location : 'yes',
    hideurlbar : 'yes',
    toolbarcolor : '#004938',
    footer : 'no',
    hidden : 'no',
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'no',
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no',
    closebuttoncaption : 'Close',
    disallowoverscroll : 'no',
    toolbar : 'yes', 
    enableViewportScale : 'no',
    allowInlineMediaPlayback : 'no', 
    presentationstyle : 'pagesheet'
};
 
  items: any;
  contents: any;
 
  constructor(private theInAppBrowser: InAppBrowser, public navCtrl: NavController, public http: Http) {
 
    this.http.get('https://bitbucket.org/skeddy_sked/configurator/raw/master/config.json').map(res => res.json()).subscribe(data => {
        this.items = data.links;
         });
    this.http.get('https://bitbucket.org/skeddy_sked/configurator/raw/master/config.json').map(res => res.json()).subscribe(data => {
        this.contents = data.contents;
        });
   }
  
  public openWithCordovaBrowser(url : string){
    let target = "_blank";
    this.theInAppBrowser.create(encodeURI(url),target,this.options);
}

}