import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
    selector: 'page-info',
    templateUrl: 'info.html'
})
export class InfoPage {

    options: InAppBrowserOptions = {
        location: 'yes',
        hideurlbar: 'yes',
        toolbarcolor: '#004938',
        footer: 'no',
        hidden: 'no',
        clearcache: 'yes',
        clearsessioncache: 'yes',
        zoom: 'no',
        hardwareback: 'yes',
        mediaPlaybackRequiresUserAction: 'no',
        shouldPauseOnSuspend: 'no',
        closebuttoncaption: 'Close',
        disallowoverscroll: 'no',
        toolbar: 'yes',
        enableViewportScale: 'no',
        allowInlineMediaPlayback: 'no',
        presentationstyle: 'pagesheet'
    };

    constructor(private theInAppBrowser: InAppBrowser, public navCtrl: NavController, public http: Http) {
    
            this.http.get('https://randomuser.me/api/?results=5')
            .map(res => res.json())
            .subscribe(res => {
            this.users=res.results;
        });
    
    }

    users:any
    load(refresher)
        {
            this.http.get('https://bitbucket.org/skeddy_sked/configurator/raw/master/content.json')
            .map(res => res.json())
            .subscribe(res => {
            this.users=res.results;
            refresher.complete();
            }, (err) => {
            alert("failed");
        });
    }
    
    clearCache()
        {
            this._users = null;
        }

    public openWithCordovaBrowser(url: string) {
        let target = "_blank";
        this.theInAppBrowser.create(encodeURI(url), target, this.options);
    }
    
}
