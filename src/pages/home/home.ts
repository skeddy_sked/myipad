import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser , InAppBrowserOptions } from '@ionic-native/in-app-browser';

@Component({
selector: 'page-home',
templateUrl: 'home.html'
})
export class HomePage {
options : InAppBrowserOptions = {
    location : 'yes',
    hideurlbar : 'yes',
    toolbarcolor : '#004938',
    footer : 'no',
    hidden : 'no',
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'no',
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no',
    closebuttoncaption : 'Close',
    disallowoverscroll : 'no',
    toolbar : 'yes', 
    enableViewportScale : 'no',
    allowInlineMediaPlayback : 'no', 
    presentationstyle : 'pagesheet'
};
constructor(private theInAppBrowser: InAppBrowser, public navCtrl: NavController) {

}
public openWithCordovaBrowser(url : string){
    let target = "_blank";
    this.theInAppBrowser.create(encodeURI(url),target,this.options);
}  

}